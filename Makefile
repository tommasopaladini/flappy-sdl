OBJS = src/main.cpp src/flappy.cpp src/texture.cpp src/timer.cpp src/column.cpp

CC = g++

INCLUDE_PATHS = -Iinclude

LIBRARY_PATHS = -Llib

LINKER_FLAGS = -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf

OBJ_NAME = bin/flappy.exe

all : $(OBJS)
	$(CC) $(OBJS) $(INCLUDE_PATHS) $(LIBRARY_PATHS) $(LINKER_FLAGS) -o $(OBJ_NAME)
