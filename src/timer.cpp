#include "timer.h"
#include <iostream>
#include <SDL2/SDL.h>
using namespace std;

Timer::Timer(){
   cout << "Instantiating timer" << endl;
   ticks = 0;
   started = false;
   paused = true;
}

Timer::~Timer(){
   //cout << "Destroying timer" << endl;
}

void Timer::start(){
   started = true;
   paused = false;
   ticks = SDL_GetTicks();
}

void Timer::stop(){
   paused = true;
}

void Timer::reset(){
   paused = true;
   started = false;
   ticks = 0;
}

unsigned long Timer::getTicks(){
   return SDL_GetTicks() - ticks;
}

bool Timer::isPaused(){
   return paused;
}

bool Timer::hasStarted(){
   return started;
}
