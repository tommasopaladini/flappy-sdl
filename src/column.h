#ifndef COLUMN_H
#define COLUMN_H

#include <SDL2/SDL.h>
#include <iostream>
#include <stdlib.h>

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;
extern SDL_Renderer* renderer;

class Column{
   private:
      short x;
      short gapPosition;
      SDL_Rect bottom_rect;
      SDL_Rect top_rect;
   public:
      Column(short);
      ~Column();
      static const int GAP_HEIGHT;
      static const int MIN_GROUND_DISTANCE;
      static const int COLUMN_VELOCITY;
      short getX(void);
      short getGapPosition(void);
      void setX(unsigned short);
      void setGapPosition(unsigned short);
      short randomGap();
      void shift(void);
      void draw(void);
      SDL_Rect getSDLTopRect();
      SDL_Rect getSDLBottomRect();
};

#endif
