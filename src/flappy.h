#ifndef FLAPPY_H
#define FLAPPY_H

#include <SDL2/SDL.h>
#include <iostream>
#include "timer.h"
extern SDL_Renderer* renderer;
extern const int SCREEN_HEIGHT;

class Flappy {
   private:
      int x;
      float y;
      bool hasJumped;
      SDL_Rect s_rect;
      Timer posTimer;
   public:
      static const int MAX_JUMP_HEIGHT;
      static const int START_X_POSITION;
      static const int START_Y_POSITION;
      static const int FALL_DISTANCE;
      static const int HEIGHT;
      static const int WIDTH;
      static const float GRAVITY_ACCELERATION;
      static const float VELOCITY;
      Flappy();
      ~Flappy();
      int getX(void);
      float getY(void);
      Timer getPosTimer(void);
      void jump(void);
      void move(void);
      void draw();
      SDL_Rect getSDLRect();
};

#endif
