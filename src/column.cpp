#include "column.h"

using namespace std;

const int Column::GAP_HEIGHT = 200;
const int Column::MIN_GROUND_DISTANCE = 20;
const int Column::COLUMN_VELOCITY = 4;

Column::Column(short x){
   cout << "Creating column.." << endl;

   this->x = x;
   this->gapPosition = randomGap();

   top_rect.x = x;
   top_rect.y = 0;
   top_rect.w = 50;
   top_rect.h = gapPosition;

   bottom_rect.x = x;
   bottom_rect.y = gapPosition + GAP_HEIGHT;
   bottom_rect.w = 50;
   bottom_rect.h = SCREEN_HEIGHT - (gapPosition + GAP_HEIGHT);
}

Column::~Column(){
   //cout << "Deleting column.." << endl;
}

short Column::getX(void){
   return x;
}

short Column::getGapPosition(void){
   return gapPosition;
}

void Column::setX(unsigned short x){
   this->x = x;
}

void Column::setGapPosition(unsigned short gapPosition){
   this->gapPosition = gapPosition;

   top_rect.y = 0;
   top_rect.h = gapPosition;

   bottom_rect.y = gapPosition + GAP_HEIGHT;
   bottom_rect.h = SCREEN_HEIGHT - (gapPosition + GAP_HEIGHT);
}

void Column::shift(void){
   //cout << "Column is shifting" << endl;
   x -= COLUMN_VELOCITY;

   if(x <= - top_rect.w){
      x = SCREEN_WIDTH;
      setGapPosition(randomGap());
   }

   top_rect.x = x;
   bottom_rect.x = x;
}

void Column::draw(void){
   SDL_SetRenderDrawColor(renderer, 0, 0xFF, 0xFF, 0xFF);
   SDL_RenderFillRect(renderer, &top_rect);
   SDL_RenderFillRect(renderer, &bottom_rect);
}

short Column::randomGap(){
   return (rand() % (int)((SCREEN_HEIGHT * 5) / 10)) + MIN_GROUND_DISTANCE;
}

SDL_Rect Column::getSDLTopRect(){
   return top_rect;
}

SDL_Rect Column::getSDLBottomRect(){
   return bottom_rect;
}
