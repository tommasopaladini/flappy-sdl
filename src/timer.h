#ifndef TIMER_H
#define TIMER_H

class Timer{
   private:
      unsigned long ticks;
      bool paused;
      bool started;
   public:
      Timer();
      ~Timer();
      static const int FRAME_RATE = 60;
      void start();
      void stop();
      void reset();
      bool isPaused();
      bool hasStarted();
      unsigned long getTicks();
};

#endif
