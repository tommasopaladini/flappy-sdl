#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <time.h>
#include <vector>
#include "flappy.h"
#include "timer.h"
#include "texture.h"
#include "column.h"
#include "collision.h"

using namespace std;

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 60;
const int SCREEN_TICKS_PER_FRAME = 1000 / SCREEN_FPS;

const char* WINDOW_TITLE = "flappy-console";

SDL_Window* window = NULL;
SDL_Surface* stage = NULL;
SDL_Renderer* renderer = NULL; //if renderer is unique then it could be used as global

const int COLUMN_NUMBER = 2;
const int COLUMN_DISTANCE = 300;

int startSDL();

int main(int argc, char* args[]){

   SDL_Event event;
   cout << "Starting SDL.." << endl;

   if(startSDL())
      cout << "SDL OK.." << endl;

   if(!SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1")){
		cerr << "Warning: linear text filtering not enabled" << endl;
	}

   cout << "Creating renderer.." << endl;
   renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
   SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

   cout << "Opening font.." << endl;
   TTF_Font* font = TTF_OpenFont( "../font/opensans.ttf", 12);

   cout << "Selecting color.." << endl;
   SDL_Color textColor = { 255, 255, 255 };

   Texture countdown;
   SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
   SDL_RenderClear(renderer);
   countdown.loadText("3", font, textColor);
   countdown.render(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
   SDL_RenderPresent(renderer);
   SDL_Delay(1000);
   SDL_RenderClear(renderer);
   countdown.loadText("2", font, textColor);
   countdown.render(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
   SDL_RenderPresent(renderer);
   SDL_Delay(1000);
   SDL_RenderClear(renderer);
   countdown.loadText("1", font, textColor);
   countdown.render(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
   SDL_RenderPresent(renderer);
   SDL_Delay(1000);
   SDL_RenderClear(renderer);

   Texture fpsText;
   Flappy flappy;
   Timer gameTimer, capTimer;
   int countedFrames = 0;
   float avgFps;
   string timeText, height;
   vector<Column> columnList;
   columnList.reserve(COLUMN_NUMBER);
   Collision collisionChecker;
   vector<Column>::iterator i;

   int colPos = SCREEN_WIDTH;
   for (int i = 0; i < COLUMN_NUMBER; i++, colPos -= COLUMN_DISTANCE){
      columnList.push_back(Column(colPos));
   }

   srand((unsigned)time(0));
   gameTimer.start();
   bool quit = false; //controlla il ciclo di gioco
   cout << "Listening.." << endl;
   while(!quit && !collisionChecker.hasCollided()){
      capTimer.start();
      while(SDL_PollEvent(&event) != 0){
         if(event.type == SDL_QUIT){
               cout << "SDL_QUIT event triggered.." << endl;
               quit = true;
         }
         else if(event.type == SDL_KEYDOWN){
            switch(event.key.keysym.sym){
                case SDLK_SPACE:
                   flappy.jump();
                   //cout << "SDLK_SPACE event triggered.." << endl;
                   break;
                default:
                cout << "SDLK event triggered.." << endl;
                break;
            }
         }
      }
      flappy.move();
      for(i = columnList.begin() ; i < columnList.end(); i++){
          i->shift();
      }

      for(i = columnList.begin() ; i < columnList.end(); i++){
          if(i->getX() <= Flappy::START_X_POSITION + Flappy::WIDTH)
            collisionChecker.detectCollision(flappy, *i);
      }

      collisionChecker.groundCollision(flappy);

      SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
      SDL_RenderClear(renderer);

      flappy.draw();

      for(i = columnList.begin() ; i < columnList.end(); i++){
          i->draw();
      }

      //Calculate average FPS
      avgFps = countedFrames / (gameTimer.getTicks() / 1000.f);

      stringstream convert;
      convert << avgFps;
      timeText = convert.str();

      fpsText.loadText(timeText.c_str(), font, textColor);
      fpsText.render(580,0);

      SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
      SDL_RenderPresent(renderer);

      //How much time did it take to get here?
      int frameTicks = capTimer.getTicks();
      //Wait if necessary
      if(frameTicks < SCREEN_TICKS_PER_FRAME)
         SDL_Delay(SCREEN_TICKS_PER_FRAME - frameTicks);
      //Next frame
      countedFrames++;
      capTimer.reset();
   }

   if(collisionChecker.hasCollided()){
      cout << "Collision!" << endl;
      Timer colTime;
      colTime.start();
      Texture collision;
      SDL_Color red = {255,0,0};
      collision.loadText("COLLISION!", font, red);
      collision.render(SCREEN_WIDTH/2 - 20, SCREEN_HEIGHT/2);
      SDL_RenderPresent(renderer);
      SDL_Delay(2000);
      /*
      for(unsigned long t = colTime.getTicks(); colTime.getTicks() - t > 5000;){
         SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
         SDL_RenderClear(renderer);
         flappy.move();
         flappy.draw();
         for(i = columnList.begin() ; i < columnList.end(); i++)
             i->draw();
         collision.render(SCREEN_WIDTH/2 - 20, SCREEN_HEIGHT/2);
         SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
         SDL_RenderPresent(renderer);
      }*/
   }
   cout << "Quitting.." << endl;

   cout<< "Deallocating font.." << endl;
   TTF_CloseFont(font);
   font = NULL;

   cout << "Deallocating renderer.." << endl;
   SDL_DestroyRenderer(renderer);
   renderer = NULL;

   cout << "Deallocating window.." << endl;
   SDL_DestroyWindow(window);
   window = NULL;

   cout << "Quitting subsystems.." << endl;
   cout << "Quitting TTF.." << endl;
   TTF_Quit();
   cout << "Quitting IMG.." << endl;
   IMG_Quit();
   cout << "Qutting SDL.." << endl;
   SDL_Quit();
   return 0;
}

int startSDL(){
   cout << "Init video.." << endl;
   if(SDL_Init(SDL_INIT_VIDEO) < 0){
      cerr << "sdl_init_error (-1) : " << SDL_GetError() << endl;
      exit(-1);
   }

   cout << "Creating window.." << endl;
   window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
   if(window == NULL){
      cerr << "null_window_error (-2) : " << SDL_GetError() << endl;
      exit(-2);
   }

   cout << "Init TTF.." << endl;
   if(TTF_Init() < 0){
      cerr << "ttf_init_error (-3) : " << TTF_GetError() << endl;
      exit(-3);
   }


   cout << "Init IMG (png|jpg).. " << endl;
   int flags=IMG_INIT_JPG|IMG_INIT_PNG;
   if((IMG_Init(flags)&flags) != flags) {
      cerr << "img_init_error (-4) : " << IMG_GetError() << endl;
      exit(-4);
   }

   cout << "Getting window surface to stage.." << endl;
   stage = SDL_GetWindowSurface(window);
   if(stage == NULL){
      cerr << "null_stage_error (-5) : " << SDL_GetError() << endl;
      exit(-5);
   }

   return 0;
}
