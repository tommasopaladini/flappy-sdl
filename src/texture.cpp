#include "texture.h"

extern SDL_Renderer* renderer;

Texture::Texture(){
   cout << "Creating texture.. " << endl;
   s_texture = NULL; //Final texture
   s_width = 0;
   s_height = 0;
}

Texture::~Texture(){
   cout << "Destroying texture.." << endl;
   if(s_texture != NULL)
      SDL_DestroyTexture(s_texture);

   s_texture = NULL;
   s_width = 0;
   s_height = 0;
}

int Texture::getWidth(){
   return s_width;
}

int Texture::getHeight(){
   return s_height;
}

void Texture::loadFile(string url){

   cout << "Loading surface for texture.." << endl;
   SDL_Surface* srf = IMG_Load(url.c_str());

   if(srf == NULL){
      cerr << "null_surface : " << IMG_GetError() << endl;
   }

   cout << "Creating texture from surface.." << endl;
   SDL_Texture* t = SDL_CreateTextureFromSurface(renderer, srf);
   if(t == NULL){
      cerr << "null_texture : " << SDL_GetError() << endl;
   }

   s_height = srf->h;
   s_width = srf->w;

   cout << "Freeing momentary surface.." << endl;
   SDL_FreeSurface(srf);
   s_texture = t;
}

void Texture::loadText(string text, TTF_Font* font, SDL_Color color){

   if(s_texture != NULL){
      SDL_DestroyTexture(s_texture);
      s_texture = NULL;
      s_height = 0;
      s_width = 0;
   }
   //cout << "Loading surface.." << endl;
   SDL_Surface * tSrf = TTF_RenderText_Solid(font, text.c_str(), color);
   if(tSrf == NULL){
      cerr << "null_texture_surface : " << TTF_GetError() << endl;
   }
   //cout << "Creating texture from surface.." << endl;
   s_texture = SDL_CreateTextureFromSurface(renderer, tSrf);
   if(s_texture == NULL){
      cerr << "null_texture : " << SDL_GetError() << endl;
   }
   //cout << "Getting height and width.." << endl;
   s_height = tSrf->h;
   s_width = tSrf->w;
   //cout << "Freeing surface.." << endl;
   SDL_FreeSurface(tSrf);
}


void Texture::render(int x, int y, SDL_Rect* clip){
   SDL_Rect renderQuad;
   renderQuad.x = x;
   renderQuad.y = y;
   renderQuad.h = s_height;
   renderQuad.w = s_width;

   if( clip != NULL ){
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy(renderer,s_texture,clip,&renderQuad);
}


void Texture::setColor(Uint8 red, Uint8 green, Uint8 blue ){
	SDL_SetTextureColorMod( s_texture, red, green, blue );
}

void Texture::setAlpha( Uint8 alpha ){
	SDL_SetTextureAlphaMod( s_texture, alpha );
}
