#include "collision.h"

Collision::Collision(){
   collision = false;
}

Collision::~Collision(){

}

void Collision::groundCollision(Flappy flappy){
   SDL_Rect flappy_rect = flappy.getSDLRect();
   if(flappy_rect.y + flappy_rect.h >= SCREEN_HEIGHT)
      collision = true;
}

void Collision::detectCollision(Flappy flappy, Column column){
   SDL_Rect flappy_rect = flappy.getSDLRect();
   SDL_Rect top_rect = column.getSDLTopRect();
   SDL_Rect bottom_rect = column.getSDLBottomRect();

   if(flappy_rect.y + flappy_rect.h >= SCREEN_HEIGHT)
      collision = true;
   else if(flappy_rect.x  >= top_rect.x &&
      flappy_rect.x <= top_rect.x + top_rect.w &&
      flappy_rect.y <= top_rect.h)
      collision = true;
   else if(flappy_rect.x + flappy_rect.w >= top_rect.x &&
      flappy_rect.x + flappy_rect.w <= top_rect.x + top_rect.w &&
      flappy_rect.y <= top_rect.h)
      collision = true;
   else if(flappy_rect.x + flappy_rect.w >= bottom_rect.x &&
      flappy_rect.x + flappy_rect.w <= bottom_rect.x + bottom_rect.w &&
      flappy_rect.y + flappy_rect.h >= bottom_rect.y)
      collision = true;
   else if(flappy_rect.x >= bottom_rect.x &&
      flappy_rect.x <= bottom_rect.x + bottom_rect.w &&
      flappy_rect.y + flappy_rect.h >= bottom_rect.y)
      collision = true;
}

bool Collision::hasCollided(){
   return collision;
}
