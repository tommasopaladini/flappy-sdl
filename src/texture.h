#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
using namespace std;

#ifndef TEXTURE_H
#define TEXTURE_H

class Texture{
   private:
      int            s_height;
      int            s_width;
      SDL_Texture*   s_texture;
   public:
      Texture();
      ~Texture();
      void loadFile(string url);
      void loadText(string text, TTF_Font* font, SDL_Color color);
      void render(int x, int y, SDL_Rect* clip = NULL);
      int getWidth();
      int getHeight();
      void setColor(Uint8 r, Uint8 g, Uint8 b);
      void setAlpha(Uint8 a);
};

#endif
