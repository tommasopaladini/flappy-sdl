#include "flappy.h"

using namespace std;

const int Flappy::START_X_POSITION = 20;
const int Flappy::START_Y_POSITION = 100;
const int Flappy::MAX_JUMP_HEIGHT = 100;
const int Flappy::FALL_DISTANCE = 2;
const int Flappy::HEIGHT = 40;
const int Flappy::WIDTH = 40;
const float Flappy::GRAVITY_ACCELERATION = 4.f;
const float Flappy::VELOCITY = 15;

Flappy::Flappy(){
   cout << "Creating Flappy.." << endl;
   x = START_X_POSITION;
   y = START_Y_POSITION;

   s_rect.x = x;
   s_rect.y = y;
   s_rect.h = HEIGHT;
   s_rect.w = WIDTH;
   posTimer.start();
   hasJumped = false;
}

Flappy::~Flappy(){
   //cout << "Deleting Flappy.." << endl;
}

int Flappy::getX(void){
   return x;
}

float Flappy::getY(void){
   return y;
}

void Flappy::move(void){

   float t = posTimer.getTicks() / 100;

   if(hasJumped)
      y = y - VELOCITY * t;
   else
      y = y + 0.5f * (GRAVITY_ACCELERATION * t);

   if(t > 1 && hasJumped){
      hasJumped = false;
      posTimer.reset();
      posTimer.start();
   }

   if (y <= 0)
      y = 0;
   else if(y >= SCREEN_HEIGHT - s_rect.h)
      y = SCREEN_HEIGHT - s_rect.h;

   s_rect.y = (int) y;
}

void Flappy::jump(void){
   cout << "Jumping.." << endl;
   if(!hasJumped){
      posTimer.reset();
      hasJumped = true;
      posTimer.start();
   }
}

void Flappy::draw(){
   SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0, 0xFF);
   SDL_RenderFillRect(renderer, &s_rect);
}

SDL_Rect Flappy::getSDLRect(){
   return s_rect;
}
