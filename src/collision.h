#ifndef COLLISION_H
#define COLLISION_H

#include "flappy.h"
#include "column.h"

extern const int SCREEN_HEIGHT;

class Collision {
   private:
      bool collision;
   public:
   Collision();
   ~Collision();
   void detectCollision(Flappy,Column);
   void groundCollision(Flappy);
   bool hasCollided();
};

#endif
